/**
 * Created by Jerome on 03-03-17.
 */
import io from 'socket.io-client';

class Client
{
    constructor()
    {
        this.players = {};

        this.localplayer = null;

        this.socket = io.connect();

        this.listeners = [];

        this.askNewPlayer= this.askNewPlayer.bind(this);
        this.addNewPlayer = this.addNewPlayer.bind(this);
        this.removePlayer = this.removePlayer.bind(this);
        this.movePlayer = this.movePlayer.bind(this);
        this.background = this.background.bind(this);

        this.socket.on('reconnect',
            (data) =>
            {
                console.log("Client reconnected.");
                for(let element in this.players)
                {
                    let player = this.players[element];
                    this.removePlayer(player.id);
                }

                this.players = {};
                this.localplayer = null;
                this.askNewPlayer();
            }
        );

        this.socket.on('newplayer',
            (data) =>
            {
                this.addNewPlayer(data.id, data.x, data.y, data.name);
            }
        );

        this.socket.on('allplayers',
            (data) =>
            {
                for (let i = 0; i < data.length; i++) {
                    this.addNewPlayer(data[i].id, data[i].x, data[i].y, data[i].name, i === data.length-1);
                }
            }
        );

        this.socket.on('move',
            (data) =>
            {
                this.movePlayer(data.id, data.x, data.y);
            }
        );


        this.socket.on('pineapple',
            (data) =>
            {
                this.pineapple(data.x, data.y);
            }
        );

        this.socket.on('collected',
            (data) =>
            {
                this.collected();
            }
        );

        this.socket.on('despawned',
            (data) =>
            {
                this.despawned();
            }
        );

        this.socket.on('presence',
            (sector) =>
            {
                this.presence(sector);
            }
        );

        this.socket.on('remove',
            (id) =>
            {
                this.removePlayer(id);
            }
        );

        this.socket.on('background',
            (color, curtain) =>
            {
                this.background(color, curtain);
            }
        );

        this.socket.on('message',
            (text, color, duration) =>
            {
                this.message(text, color, duration);
            }
        );
    }

    removePlayer(id)
    {
        let player = this.players[id];
        for (let listener of this.listeners) listener.removeplayer(player);
        delete this.players[id];
    };

    background(color, curtain)
    {
        console.log("Client: background");
        for (let listener of this.listeners) listener.background(color, curtain);
    };

    presence(sectors)
    {
        console.log("Client: presence " + sectors );
        for (let listener of this.listeners) listener.presence(sectors);
    };

    message(text, color, duration)
    {
        console.log("Client: message " + text);
        for (let listener of this.listeners) listener.message(text, color, duration);
    };

    movePlayer(id,x,y)
    {
        if (!this.players[id])
            this.addNewPlayer(id, x, y);

        this.players[id].x = x;
        this.players[id].y = y;

        for (let listener of this.listeners) listener.move(id, x, y);
    };

    addNewPlayer(id, x, y, name, local)
    {
        console.log("client: " + name + " connected.");
        let player = {"id":id, "name":name, "x":x, "y":y, "a": (Math.random()-0.5)*Math.PI*2};

        if (!this.players[id])
        {
            this.players[id] = player;

            if (local) this.localplayer = player;

            for (let listener of this.listeners) listener.newplayer(player);
        }
        else
        {
            console.log("Client: duplicate player received.");
        }
    }

    askNewPlayer(name)
    {
        if (name) this.name = name;

        this.socket.emit('register', this.name);
    }

    sendClick(x, y)
    {
        this.socket.emit('click', {x: x, y: y});
    }

    sendCollected()
    {
        console.log("Client: collected");
        this.socket.emit('collected');
    }


    pineapple(x, y)
    {
        console.log("Client: pineapple");
        for (let listener of this.listeners) listener.pineapple(x,y);
    }

    collected()
    {
        console.log("Client: collected");
        for (let listener of this.listeners) listener.collected_pineapple();
    }

    despawned()
    {
        console.log("Client: despawned pineapple");
        for (let listener of this.listeners) listener.despawn_pineapple();
    }

}

export default new Client();
