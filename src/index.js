import Boot from 'states/Boot';
import Preload from 'states/Preload';
import GameTitle from 'states/GameTitle';
import Main from 'states/Main';
import GameOver from 'states/GameOver';
import Alertify from 'alertify.js';

function setCookie(cname, cvalue, exdays)
{
    let  d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let  expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let  name = cname + "=";
    let  ca = document.cookie.split(';');
    for(let  i = 0; i < ca.length; i++) {
        let  c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


class Game extends Phaser.Game {

    constructor() {
        super(1200, 640, Phaser.AUTO);

        this.state.add('Boot', Boot, false);
        this.state.add('Preload', Preload, false);
        this.state.add('GameTitle', GameTitle, false);
        this.state.add('Main', Main, false);
        this.state.add('GameOver', GameOver, false);

        this.requestName = this.requestName.bind(this);

        this.playername = "";
    }

    requestName()
    {
        Alertify.defaultValue(getCookie("user"));
        Alertify.prompt("Bitte Namen eingeben",
            (name) =>
            {
                this.playername = name;
                if (name)
                {
                    setCookie("user", name, 1);
                    this.scale.startFullScreen(false);
                    this.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
                    this.state.start('Boot');
                }
                else this.requestName();
            },
            this.requestName
        );
    }
}

let game = new Game();
game.requestName();
