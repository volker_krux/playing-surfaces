import Client from "client";

const font_big = "60px Arial";
const font_small = "22px Arial";
const curtain_color = 0x000000;

    class PlayableState extends Phaser.State
{
    player;

    create()
    {
        this.newplayer = this.newplayer.bind(this);
        this.removeplayer = this.removeplayer.bind(this);
        this.background = this.background.bind(this);
        this.move= this.move.bind(this);
        this.ondown= this.ondown.bind(this);
        this.presence = this.presence.bind(this);
        this.message = this.message.bind(this);
        this.pineapple = this.pineapple.bind(this);
        this.despawn_pineapple = this.despawn_pineapple.bind(this);
        this.collected_pineapple = this.collected_pineapple.bind(this);

        this.game.stage.backgroundColor = 0;

        this.quadrants = [];
        this.fades = [];
        this.tints = [];

        this.colors = [0x903030, 0x309030, 0xC0C030, 0x303090];
        this.neutral = 0x505070;
        this.dead = 0x404040;

        for (let i = 0; i < 2; i++)
        {
            for (let j = 0; j < 2; j++) {
                let quadrant = this.game.add.tileSprite(this.game.world.width / 2 * i + 10, j * this.game.world.height / 2 + 10, this.game.world.width / 2 - 20, this.game.world.height / 2 - 20, 'water');
                this.quadrants.push(quadrant);
                quadrant.alpha = 0;
                quadrant.tint = this.neutral;
            }
        }

        this.playerLayer = this.game.add.group();
        this.curtainLayer = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'water');
        this.curtainLayer.tint = curtain_color;
        this.textLayer = this.game.add.group();

        //Message text
        let  text = "";
        let  style = { font: font_big, fill: "#ff0044", align: "center" };
        this.text =  this.game.make.text(0,0, text, style);
        this.textLayer.add(this.text);

        //Register with client
        Client.listeners.push(this);


        //Load all existing players
        for (let id in Client.players)
        {
            let player = Client.players[id];
            this.newplayer(player);
        }

        //this.game.input.activePointer.leftButton.onDown.add(this.ondown);
        this.game.input.onTap.add(this.ondown, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.ONE).onDown.add(()=>{Client.socket.emit("warp-setup")}, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.TWO).onDown.add(()=>{Client.socket.emit("warp-prelude")}, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.THREE).onDown.add(()=>{Client.socket.emit("warp-mode1")}, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.FOUR).onDown.add(()=>{Client.socket.emit("warp-surfaces")}, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.FIVE).onDown.add(()=>{Client.socket.emit("warp-mode2")}, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.SIX).onDown.add(()=>{Client.socket.emit("warp-pineapple")}, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.SEVEN).onDown.add(()=>{Client.socket.emit("warp-gameover")}, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.EIGHT).onDown.add(()=>{Client.socket.emit("warp-lastapple")}, this);
        this.game.input.keyboard.addKey(Phaser.Keyboard.ZERO).onDown.add(()=>{Client.socket.emit("warp-finalcurtain")}, this);
    }

    shutdown()
    {
        Client.listeners = [];

        for (let id in Client.players)
        {
            let player = Client.players[id];
            player.a = player.boat.rotation;
        }
    }


    static getShortestAngle (angle1, angle2) {

        let  difference = angle2 - angle1;

        if (difference === 0)
        {
            return 0;
        }

        let  times = Math.floor((difference - (-Math.PI)) / (Math.PI*2));

        return difference - (times * Math.PI * 2);
    }


    move(id,x,y)
    {
        let player = Client.players[id];
        let sprite = player.sprite;
        let boat = player.boat;

        let point = new Phaser.Point(x,y);

        if (Client.players[id].tween)
        {
            Client.players[id].tween.stop();
            //TODO: less abrupt stoppage? sprite.deltaX ... ?
        }

        let direction = sprite.position.angle(point);
        let shortest = PlayableState.getShortestAngle(direction, boat.rotation);
        let delay = Math.abs(500 * shortest / Math.PI);

        this.game.add.tween(player.boat).to( {rotation: direction}, delay, Phaser.Easing.Sinusoidal.InOut, true);

        //Start new movement tween
        Client.players[id].tween = this.game.add.tween(sprite).to( { x:point.x, y:point.y }, 3000, Phaser.Easing.Quadratic.InOut, true, delay);
    }

    newplayer(player)
    {
        let colors = [0xFFFF00, 0xFF6060, 0x60FF60, 0x6060FF, 0xFF00FF, 0x00FFFF, 0xC06060, 0xC03030, 0x30c030, 0x3030c0];

        if (!player.sprite)
        {
            console.log("PlayableState: newplayer");
            player.sprite = this.game.make.sprite(player.x/**this.game.world.width*/, player.y/*this.game.world.height*/);
            this.playerLayer.add(player.sprite);


            player.wobbler = this.game.make.sprite(0,0);
            player.sprite.addChild(player.wobbler);

            player.boat = this.game.make.sprite(0, 0, "player" + player.id % this.game.ships);
            player.wobbler.addChild(player.boat);
            player.boat.anchor.x = 0.5;
            player.boat.anchor.y = 0.5;
            player.boat.rotation = player.a;
            player.boat.tint = colors[player.id%colors.length];

            let style = { font: font_small, fill: "#CCCCCC", align: "center" };
            player.nametag = this.game.make.text(0, 40, player.name, style);
            player.nametag.anchor.set(0.5);
            player.nametag.setShadow(2,2);

            player.wobbler.addChild(player.nametag);
            this.game.add.tween(player.wobbler).to( { y: 10}, 700, Phaser.Easing.Sinusoidal.InOut, true, Math.random()*700, -1, true);

            if (player === Client.localplayer)
            {
                this.player = player;
                this.game.add.tween(player.boat).to( { alpha: 0.5 }, 200, Phaser.Easing.Linear.None, true, 0, -1);
            }
        }
        else
        {
            console.log("This player already had a sprite.");
        }
    }

    removeplayer(player)
    {
        console.log("Player dropped! ", player.id);
        this.playerLayer.remove(player.sprite);

        if (this.player === player)
        {
            this.player = undefined;
        }
    }

    ondown()
    {
        let x = this.game.input.activePointer.x;
        let y = this.game.input.activePointer.y;

        Client.sendClick(x, y);
    }

    background(color, curtain)
    {
        this.tweenBackground(this.game.stage, this.game.stage.backgroundColor, color, 1000);
        this.game.add.tween(this.curtainLayer).to( {alpha: curtain || 0}, 1000, Phaser.Easing.Linear.None, true);
    }

    tweenBackground(obj, startColor, endColor, time = 250, delay = 0, callback = null) {
        // check if is valid object
        if (obj)
        {
            // create a step object
            let colorBlend = {
                step: 0
            };

            // create a tween to increment that step from 0 to 100.
            let colorTween = this.game.add.tween(colorBlend).to({ step: 100 }, time, Phaser.Easing.Sinusoidal.InOut, true, delay);

            // add an anonomous function with lexical scope to change the tint, calling Phaser.Colour.interpolateColor
            colorTween.onUpdateCallback(() => {
                obj.backgroundColor = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step);
            });

            // set object to the starting colour
            obj.tint = startColor;

            // if you passed a callback, add it to the tween on complete
            if (callback) {
                colorTween.onComplete.add(callback, this);
            }

            // finally, start the tween
            colorTween.start();

            return colorTween;
        }
    }



    tweenTint(obj, startColor, endColor, time = 250, delay = 0, callback = null) {
        // check if is valid object
        if (obj)
        {
            // create a step object
            let colorBlend = {
                step: 0
            };

            // create a tween to increment that step from 0 to 100.
            let colorTween = this.game.add.tween(colorBlend).to({ step: 100 }, time, Phaser.Easing.Sinusoidal.InOut, true, delay);

            // add an anonomous function with lexical scope to change the tint, calling Phaser.Colour.interpolateColor
            colorTween.onUpdateCallback(() => {
                obj.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step);
            });

            // set object to the starting colour
            obj.tint = startColor;

            // if you passed a callback, add it to the tween on complete
            if (callback) {
                colorTween.onComplete.add(callback, this);
            }

            // finally, start the tween
            colorTween.start();

            return colorTween;
        }
    }

    presence(sectors)
    {
        for (let i = 0; i < this.quadrants.length; i++)
        {
            if (this.fades[i]) this.fades[i].stop(true);
            if (this.tints[i]) this.tints[i].stop(true);

            let intensity = sectors[i];

            if (intensity === -1) //dead sector
            {
                this.fades[i] = this.game.add.tween(this.quadrants[i]).to( {alpha: 0}, 500, Phaser.Easing.Bounce.Out, true, 500);
                this.tints[i] = this.tweenTint(this.quadrants[i], this.quadrants[i].tint, this.dead, 500);
            }
            else if (intensity < 1) //weighted presence
            {
                this.fades[i] = this.game.add.tween(this.quadrants[i]).to( {alpha: Math.max(intensity, 0.2)}, 1000, Phaser.Easing.Linear.None, true);
                this.tints[i] = this.tweenTint(this.quadrants[i], this.quadrants[i].tint, this.neutral, 500);
            }
            else //full presence
            {
                this.fades[i] = this.game.add.tween(this.quadrants[i]).to( {alpha: 0.9}, 1000, Phaser.Easing.Bounce.InOut, true, 500);
                this.tints[i] = this.tweenTint(this.quadrants[i], this.quadrants[i].tint, this.colors[i], 500, 500);
            }
        }
    }


    pineapple(x, y)
    {
        if (this.pickup)
        {
            this.game.world.remove(this.pickup);
        }

        console.log("PlayableState: pineapple spawned");
        this.pickup = this.game.add.sprite(x, y, "pineapple");
        this.pickup.anchor.x = 0.5;
        this.pickup.anchor.y = 0.5;

        this.game.add.tween(this.pickup.scale).from( { x: 10, y: 10}, 1000, Phaser.Easing.Bounce.Out, true);
    }

    despawn_pineapple()
    {
        if (this.pickup)
        {
            let temp_pickup = this.pickup;
            this.pickup = undefined;
            this.game.add.tween(temp_pickup).to( { alpha : 0}, 700, Phaser.Easing.Sinusoidal.Out, true);
        }
    }

    collected_pineapple()
    {
        if (this.pickup)
        {
            let temp_pickup = this.pickup;
            this.pickup = undefined;

            this.game.add.tween(temp_pickup).to( { x: this.game.world.centerX, y: this.game.world.centerY}, 700, Phaser.Easing.Back.InOut, true);
            this.game.add.tween(temp_pickup).to( { angle : 1125}, 1000, Phaser.Easing.Back.InOut, true);
            this.game.add.tween(temp_pickup.scale).to( { x: 3, y: 3}, 800, Phaser.Easing.Back.Out, true);
            let tween = this.game.add.tween(temp_pickup).to( { tint : 0, alpha: 0}, 400, Phaser.Easing.Sinusoidal.In, true, 1400);
            tween.onComplete.add(()=>{this.game.world.remove(temp_pickup);});
        }
    }

    message(text, color, duration)
    {
        if (duration >= 6000)
        {
            //We do this so texts can fade out before next texts are shown
            duration = duration - 2000;
        }
        else
        {
            duration = 4000;
        }

        if (this.text.tween1) this.text.tween1.stop();
        if (this.text.tween2) this.text.tween2.stop(true);

        if (text)
        {
            let style = { font: font_big, fill: color, boundsAlignH: "center", boundsAlignV: "middle", align: "center"};
            this.text.setStyle(style);
            this.text.setText(text);

            this.text.setTextBounds(0, this.game.world.centerY, this.game.world.width, this.game.world.height/2);
            this.text.alpha = 1;
            this.text.tween2 = this.game.add.tween(this.text).to({alpha: 0.0}, 1000, Phaser.Easing.Sinusoidal.In, true, duration);
            this.text.setShadow(4,4);
            this.text.alpha = 0;
            this.text.tween1 = this.game.add.tween(this.text).to({alpha: 1.0}, 500, Phaser.Easing.Sinusoidal.Out, true);
        }
        else
        {
            this.text.alpha = 0;
        }
    }

    update()
    {
        if (this.player && this.pickup)
        {
            if (this.player.sprite.position.distance(this.pickup.position) < this.pickup.width/2/this.pickup.scale.x)
            {
                Client.sendCollected();
                this.collected_pineapple();
            }
        }
    }
}

export default PlayableState;
