import Client from "client";
import PlayableState from "./PlayableState";

class GameTitle extends PlayableState
{
    create()
    {
        super.create();

        var text = "";
        var style = { font: "65px Arial", fill: "#ff0044", align: "center" };
        this.text =  this.game.add.text(this.game.world.centerX - this.game.world.width/2, 0, text, style);

        //TODO: adapt if spectating.
        Client.askNewPlayer();
    }


    startgame()
    {
        super.startgame();
        this.game.state.start("Main");
    }

    newplayer(player)
    {
        super.newplayer(player);

        this.text.text = Object.keys(Client.players).length + " players connected";
    }

    removeplayer(player)
    {
        super.removeplayer(player);
        this.text.text = Object.keys(Client.players).length + " players connected";
    }
}

export default GameTitle;
