class Preload extends Phaser.State
{
	preload()
    {
        let ships = [
            "images/sprites/boat.png"
        ];

//        this.game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');

        this.game.stage.backgroundColor = '#FF0F0F';

        let i = 0;
        for (let ship of ships)
        {
            this.game.load.image('player' + i++, ship);
        }

        this.game.ships = i;

		/* Preload required assets */
        this.game.load.image('pineapple','images/sprites/pineapple.png');

        this.game.load.image('water','images/sprites/tile_back.png');
	}

	create() {
		//NOTE: Change to GameTitle if required
        //this.game.state.start("GameTitle");
        this.game.state.start("Main");
	}

}

export default Preload;
