import Client from "client";
import PlayableState from "./PlayableState";

class Main extends PlayableState
{
    create() {
        super.create();

        Client.askNewPlayer(this.game.playername);
    }

}

export default Main;
