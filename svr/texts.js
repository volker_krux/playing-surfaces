let texts =
    {
        hints_store:
            [       //90s
                ["Don't collect the Pineapple.", "#FFCC60", 10000],
                ["Don't use your phone in concert halls!", "#FF6060", 10000],
                ["Please ignore the Pineapple.", "#FFCC60", 10000],
                ["To make your pineapple soft and juicy,\nkeep it at room temperature for\n1 or 2 days before cutting.", "#FFFFFF", 10000],
                ["Don't collect the Pineapple.", "#FFCC60", 10000],
                ["It takes almost 3 years\nfor a single pineapple\nto reach maturity", "#FFFFFF", 10000],
                ["A pineapple is the result of\nmany flowers whose fruitlets\nhave joined around the core.", "#FFFFFF", 10000],
                ["One cup of pineapple has 70 to 85 calories!\nBetter stay away from it.", "#FFFFFF", 10000],
                ["Yeah, stay away from Pineapples.", "#FFCC60", 10000]
            ],


        mode1_store:
            [  //30s
                ["Loading...", "#FFFFFF", 20000],
                ["Ready?", "#FFFFFF", 5000],
                ["Get set!", "#FFFFFF", 3000],
                ["Not yet!", "#CCCCCC", 1000],
                ["Count of three", "#CCCCCC", 1000],
                ["One", "#CCCCCC", 1000],
                ["Two", "#CCCCCC", 1000],
                ["2 ¾", "#CCCCCC", 1000],
                ["soon...", "#CCCCCC", 1000],
                ["Let's go!", "#FFFFFF", 50]
            ],

        mode2_store:
            [
                ["YOU LOST", "#FFFFFF", 1000],
                ["DO NOT COLLECT THE PINEAPPLE", "#FFFFFF", 4000],
                ["Let's try again.", "#FFFFFF", 10000],
                ["Ready", "#FFFFFF", 2000],
                ["Go!", "#FFFFFF", 2000],
            ],


        score_store:
            [
                ["YOU LOST", "#FFFFFF", 5000],
                ["Player %n collected", "#FFFFFF", 5000],
                ["%i Pineapples", "#FFFFFF", 5000],
                ["Thanks for screwing things up.", "#FFFFFF", 5000],
                ["Player %n, please sing:", "#FFFFFF", 5000],
                ["'____________________________'", "#FFFFFF", 20000],
                [".... now!", "#FFFFFF", 5000],
                ["Player %n gets a cookie:", "#FFFFFF", 5000],
            ],
    };
module.exports = texts;
