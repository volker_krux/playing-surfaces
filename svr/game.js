let io;

let texts = require("./texts");

const states =
    {
        "setup" : {"background":0x000040},
        "prelude" : {"fun": sendMessage, "first":1000, "time" : 10000, "init": ()=>{messages=Array.from(texts.hints_store)}, "next": "countdown1", "background": 0x404040, "curtain":0.3},
        "countdown1" : {"fun": sendMessage, "time" : 2000, "init": ()=>{messages=Array.from(texts.mode1_store)},  "next": "surfaces", "background":0x002040, "curtain":1},
        "surfaces": {"fun": updatePresence, "time": 5000, "init": ()=>{activations=[0,0,0,0]; presence=[0,0,0,0]; io.emit("presence", presence);}, "background":0x002040, "next":"last30", "hasOnePineapple" : true, "hastLast30" : true},
        "last30": {"fun": finishLast30, "time": 30000, "background":0x002040, "next":"countdown2"},
        "countdown2" : {"fun": sendMessage, "time" : 2000, "init": ()=>{messages=Array.from(texts.mode2_store)},  "next": "pineapple", "background":0x005070, "curtain":1},
        "pineapple" : {"fun": updatePresence, "time" : 5000, "init":()=>{activations=[0,0,0,0]; presence=[0,0,0,0]; io.emit("presence", presence); pineapple_spawns = 5; pineappleTimeout = setTimeout(updatePineapple, 2000); pineapple_next="gameover"}, "background":0x004020},
        "gameover" : {"fun": sendMessage, "time" : 2000, "init": ()=>{messages=Array.from(texts.score_store)},  "next": "lastapple", "background":0x005070,"curtain":1},
        "lastapple" : {"fun": updatePineapple, "time" : 120000, "init":()=>{pineapple_spawns = 1; pineapple_next="finalcurtain"}, "background":0x505050}, //TODO: 100s you lost message.
        "finalcurtain" : {"background":0x0,"curtain":1},
    };

let width = 1200;
let height = 640;

let lastPlayerID;
let players;
let activations;
let presence;

let messages;

let background;

let gameState;
let timeout;

let pineappleTimeout;
let pineapple_spawns;
let pineapple_next;
let pineapple;

function cleanup()
{
    //Clear Message Display
    io.emit("message");

    //Stop State Machine
    if (timeout) clearTimeout(timeout);
    timeout = undefined;

    //StopPineappleSpawns
    if (pineappleTimeout) clearTimeout(pineappleTimeout);
    pineappleTimeout = undefined;

    //Despawn existing pineapple
    pineapple = false;
    pineapple_spawns = 0;
    pineapple_next = undefined;
    io.emit("despawned");

    //Clear playing field
    activations = [0, 0, 0, 0];
    presence = [-1,-1,-1,-1];
    io.emit("presence", presence);
}

function reset()
{
    cleanup();

    for (let player of getAllPlayers())
    {
        io.emit('remove', player.id);
    }

    io.emit('allplayers',getAllPlayers());
    stateMachine("setup");
}

function setup(svr)
{
    lastPlayerID = 0;
    players = 0;

    io = require('socket.io').listen(svr);

    io.on('connection',function(socket){
        socket.on('register',function(name){
            if (typeof name !== "string" ) return;
            if (name.length < 1) return;

            if (name.length > 12)
            {
                name = name.substring(0, 10)+"...";
            }
            players++;
            socket.player = {
                id: lastPlayerID++,
                name: name,
                x: randomInt(width * 0.2, width * 0.8),
                y: randomInt(height * 0.2, height * 0.8)
            };
            console.log("Server: New Player ", socket.player);

            //Send this player a list of all the players
            welcome(socket);

            //Send the others TODO: CHECK! - info about this new player
            socket.broadcast.emit('newplayer', socket.player);

            socket.on('click',function(data){
                console.log('Server: Click to '+data.x+', '+data.y);
                if (data.x < 0 || data.x > width) return;
                if (data.y < 0 || data.y > height) return;

                socket.player.x = data.x;
                socket.player.y = data.y;

                io.emit('move', socket.player);
            });

            socket.on('collected',function(){
                console.log('Server: Pineapple collected');
                collect(socket.player);
                io.emit('collected');
            });

            socket.on('warp-setup',function(){
                console.log('Server: Resetting!');
                reset();
            });

            socket.on('warp-prelude',function(){
                cleanup();
                stateMachine("prelude");
            });

            socket.on('warp-mode1',function(){
                cleanup();
                stateMachine("countdown1");
            });

            socket.on('warp-surfaces',function(){
                cleanup();
                stateMachine("surfaces");
            });

            socket.on('warp-mode2',function(){
                cleanup();
                stateMachine("countdown2");
            });

            socket.on('warp-pineapple',function(){
                cleanup();
                stateMachine("pineapple");
            });

            socket.on('warp-gameover',function(){
                cleanup();
                stateMachine("gameover");
            });

            socket.on('warp-lastapple',function(){
                cleanup();
                stateMachine("lastapple");
            });

            socket.on('warp-finalcurtain',function(){
                cleanup();
                stateMachine("finalcurtain");
            });

            socket.on('disconnect',function(){
                players--;
                console.log("Server: Disconnected ", socket.player);
                io.emit('remove', socket.player.id);
            });
        });
    });

    reset();
}

//Welcoming new player with a nice list of things... depending on gamestate?
function welcome(socket)
{
    "use strict";
    socket.emit('allplayers',getAllPlayers());
    socket.emit('background', gameState.background, gameState.curtain);
    socket.emit('presence', presence);
}

//Collectible collected
function collect(player)
{
    "use strict";
    if (pineapple)
    {
        pineapple = false;
        player.score += 1;

        if (pineapple_next)
        {
            stateMachine(pineapple_next);
        }
    }
}


function stateMachine(state, duration)
{
    let new_state = state && states[state];

    if (new_state)
    {
        console.log("Server: Transitioned into GAMESTATE " + state);

        if (new_state.init) new_state.init();

        io.emit("background", new_state.background, new_state.curtain);

        gameState = new_state;
    }

    if (gameState)
    {
        let time = duration || gameState.time;

       if (new_state && new_state.first)
        {
            time = new_state.first;
        }

        let fun = gameState.fun;
        let parameter = gameState.parameter;

        //Only if both are given we do something.
        if (time && fun)
        {
            timeout = setTimeout(fun, time, parameter);
        }
    }
    else
    {
        console.log("Server: NULL STATE.");
    }
}


function getAllPlayers(){
    let players = [];
    Object.keys(io.sockets.connected).forEach(function(socketID){
        let player = io.sockets.connected[socketID].player;
        if(player) players.push(player);
    });
    return players;
}


function updatePresence()
{
    let players = getAllPlayers();

    //Count presence
    let presence_old = presence;
    presence = [0,0,0,0];
    for (let player of players)
    {
        let sector = (player.y < 0.5*height ? 0 : 1);
        sector = sector + (player.x < 0.5 * width ? 0 : 2);
        presence[sector] += 1 / players.length;
    }

    let majority = 0.5;
    const activations_max = 3;

    //Determine and count activations
    for (let i = 0; i < presence.length; i++)
    {
        if (activations[i] < activations_max)
        {
            if (presence[i] > majority)
            {
                presence[i] = 1;

                //Count new activation.
                if (presence_old[i] !== 1)
                {
                    activations[i] += 1;
                }
            }
        }
        else
        {
            //Too many activations
            presence[i] = -1;
        }
    }

    //TODO: Pineapple spawning in pineapple mode

    //Determine if we're full
    let filled = presence.reduce((sum, value) => sum + (value === -1 ? 1 : 0), 0);

    if (gameState.hastLast30 && filled >= 3)
    {
        //Kill all except for the last one.
        for (let i = 0; i < presence.length; i++)
        {
            //Activate the last field
            if (presence[i] >= 0) presence[i] = 1;
        }
        stateMachine(gameState.next)
    }
    else if (gameState.hasOnePineapple && filled >= 2)
    {
        if (!pineappleTimeout && !pineapple)
        {
            console.log("server: enough activations, engage pineapple spawning now.")
            spawnPineapple();
            pineappleTimeout = setTimeout(()=> {io.emit("despawned")}, 20000); //Special despawn that jams this
        }
        stateMachine();
    }
    else
    {
        stateMachine();
    }

    //Send the results.
    io.emit('presence', presence);
}


function spawnPineapple()
{
    "use strict";
    let coords = {"x": randomInt(width * 0.2, width * 0.8), "y": randomInt(height * 0.2, height * 0.8)};

    //Try to find a nice spot!
    for (let i = 200 ; i >= 0 ; i--)
    {
        let ok = true;
        for (let player of getAllPlayers())
        {
            ok &= (Math.abs(player.x - coords.x) > i) && (Math.abs(player.y - coords.y) > i);
        }

        if (!ok)
        {
            coords = {"x": randomInt(width * 0.2, width * 0.8), "y": randomInt(height * 0.2, height * 0.8)};
        }
        else
        {
            break;
        }
    }
    io.emit("pineapple", coords);
    pineapple = true;
}

function updatePineapple()
{
    pineapple_spawns--;

    if (pineapple_spawns > 0)
    {
        spawnPineapple();

        //Self-expiring pineapples here
        pineappleTimeout = setTimeout(()=> {pineapple = false; io.emit("despawned")}, 15000);
        stateMachine();
    }
    else if (pineapple_spawns === 0)
    {
        spawnPineapple();
        stateMachine();
    }
    else
    {
        stateMachine(gameState.next);
    }
}

function finishLast30()
{
    cleanup();
    stateMachine(gameState.next);
}

function sendMessage()
{
    if (messages.length > 0)
    {
        let message = messages.shift();
        io.emit("message", message[0], message[1], message[2]);
        stateMachine(undefined, message[2]);
    }
    else
    {
        stateMachine(gameState.next);
    }
}


function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low)) + low;
}


let game =
    {
        'setup' : setup
    };
module.exports = game;
