let del = require('del');
let gulp = require('gulp');
let path = require('path');
let argv = require('yargs').argv;

let source = require('vinyl-source-stream');

let gutil = require('gulp-util');
let buffer = require('gulp-buffer');
let uglify = require('gulp-uglify');
let gulpif = require('gulp-if');
let babel = require("gulp-babel");
let sourcemaps = require("gulp-sourcemaps");
let concat = require("gulp-concat");

let exorcist = require('exorcist');
let babelify = require('babelify');
let browserify = require('browserify');
let browserSync = require('browser-sync');

/**
 * Using different folders/file names? Change these constants:
 */
let PHASER_PATH = './node_modules/phaser/build/';
let SOCKET_PATH = './node_modules/socket.io-client/dist/';
let ALERTIFY_PATH = './node_modules/alertify/src';
let BUILD_PATH = './dist';
let SCRIPTS_PATH = BUILD_PATH + '/javascripts';
let SOURCE_PATH = './src';
let STATIC_PATH = './public';
let ENTRY_FILE = SOURCE_PATH + '/index.js';
let OUTPUT_FILE = 'game.js';

let keepFiles = false;

/**
 * Simple way to check for development/production mode.
 */
function isProduction() {
    return argv.production;
}

/**
 * Logs the current build mode on the console.
 */
function logBuildMode() {
    
    if (isProduction()) {
        gutil.log(gutil.colors.green('Running production build...'));
    } else {
        gutil.log(gutil.colors.yellow('Running development build...'));
    }

}

/**
 * Deletes all content inside the './build' folder.
 * If 'keepFiles' is true, no files will be deleted. This is a dirty workaround since we can't have
 * optional task dependencies :(
 * Note: keepFiles is set to true by gulp.watch (see serve()) and reseted here to avoid conflicts.
 */
function cleanBuild() {
    if (!keepFiles) {
        del(['dist/**/*.*']);
    } else {
        keepFiles = false;
    }
}

/**
 * Copies the content of the './static' folder into the '/build' folder.
 * Check out README.md for more info on the '/static' folder.
 */
function copyStatic() {
    return gulp.src(STATIC_PATH + '/**/*')
        .pipe(gulp.dest(BUILD_PATH));
}

/**
 * Copies required Phaser files from the './node_modules/Phaser' folder into the './build/scripts' folder.
 * This way you can call 'npm update', get the lastest Phaser version and use it on your project with ease.
 */
function copyPhaser() {

    let srcList = ['phaser.min.js', 'socket.io.js', 'alertify.js'];
    
    if (!isProduction()) {
        srcList.push('phaser.map', 'phaser.js');
    }
    
    srcList = srcList.map(function(file) {
        return PHASER_PATH + file;
    });
        
    return gulp.src(srcList)
        .pipe(gulp.dest(SCRIPTS_PATH));

}

/**
 * Transforms ES2015 code into ES5 code.
 * Optionally: Creates a sourcemap file 'game.js.map' for debugging.
 * 
 * In order to avoid copying Phaser and Static files on each build,
 * I've abstracted the build logic into a separate function. This way
 * two different tasks (build and fastBuild) can use the same logic
 * but have different task dependencies.
 */
function build() {
    //return gulp.src("src/**/*.js")
    /*    .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(concat(OUTPUT_FILE))
        .pipe(sourcemaps.write("."))
        .pipe(gulpif(isProduction(), uglify()))
        .pipe(gulp.dest(SCRIPTS_PATH));
    */

    let sourcemapPath = SCRIPTS_PATH + '/' + OUTPUT_FILE + '.map';
    logBuildMode();

    return browserify({
        paths: [ path.join(__dirname, 'src') ],
        entries: ENTRY_FILE,
        debug: true
    })
        .transform(babelify)
        .bundle().on('error', function(error){
            gutil.log(gutil.colors.red('[Build Error]', error.message));
            this.emit('end');
        })
        .pipe(gulpif(!isProduction(), exorcist(sourcemapPath)))
        .pipe(source(OUTPUT_FILE))
        .pipe(buffer())
        .pipe(gulpif(isProduction(), uglify()))
        .pipe(gulp.dest(SCRIPTS_PATH));
}

/**
 * Starts the Browsersync server.
 * Watches for file changes in the 'src' folder.
 */
function serve() {
    
    let options = {
        server: {
            baseDir: BUILD_PATH
        },
        open: true // Change it to true if you wish to allow Browsersync to open a browser window.
    };
    
    //browserSync(options);
    
    // Watches for changes in files inside the './src' folder.
    gulp.watch(SOURCE_PATH + '/**/*.js', ['watch-js']);
    
    // Watches for changes in files inside the './static' folder. Also sets 'keepFiles' to true (see cleanBuild()).
    gulp.watch(STATIC_PATH + '/**/*', ['watch-static']).on('change', function() {
        keepFiles = true;
    });

}


gulp.task('cleanBuild', cleanBuild);
gulp.task('copyStatic', ['cleanBuild'], copyStatic);
gulp.task('copyPhaser', ['copyStatic'], copyPhaser);
gulp.task('build', ['copyPhaser'], build);
gulp.task('fastBuild', build);
gulp.task('serve', ['build'], serve);
gulp.task('watch-js', ['fastBuild'], browserSync.reload); // Rebuilds and reloads the project when executed.
gulp.task('watch-static', ['copyPhaser'], browserSync.reload);

/**
 * The tasks are executed in the following order:
 * 'cleanBuild' -> 'copyStatic' -> 'copyPhaser' -> 'build' -> 'serve'
 * 
 * Read more about task dependencies in Gulp: 
 * https://medium.com/@dave_lunny/task-dependencies-in-gulp-b885c1ab48f0
 */
gulp.task('default', ['build']);
